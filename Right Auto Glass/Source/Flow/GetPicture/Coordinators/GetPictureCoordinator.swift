//
//  GetPictureCoordinator.swift
//  Right Auto Glass
//
//  Created by Katya on 25.04.2018.
//

import Foundation

protocol GetPictureCoordinatorOutput: Coordinator {
    
}

final class GetPictureCoordinator: BaseCoordinator, GetPictureCoordinatorOutput {
    // MARK: - Output
    
    // MARK: - Init injection
    private let factory: GetPictureModuleFactoryType
    private let router: RouterType
    
    // MARK: - Lifecycle
    init(factory: GetPictureModuleFactoryType, router: RouterType) {
        self.factory = factory
        self.router = router
    }
    
    override func start() {
        showScreenTakePhoto()
    }
}

extension GetPictureCoordinator {
    private func showScreenTakePhoto() {
        let getPictureOutput = factory.makeGetPictureScreenOutput()
        router.setRootModule(getPictureOutput)
    }
}
