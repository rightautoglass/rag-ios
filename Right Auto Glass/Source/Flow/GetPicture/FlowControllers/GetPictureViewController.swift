//
//  GetPictureViewController.swift
//  Right Auto Glass
//
//  Created by Katya on 25.04.2018.
//

import UIKit
import SVProgressHUD

class GetPictureViewController: BaseViewController {
    
    @IBOutlet weak var windshieldButton: UIButton!
    @IBOutlet weak var numberButton: UIButton!
    @IBOutlet weak var vinNumberButton: UIButton!
    @IBOutlet weak var trimButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var windshielDeleteButton: UIButton!
    @IBOutlet weak var numberDeleteButton: UIButton!
    @IBOutlet weak var vinNumberDeleteButton: UIButton!
    @IBOutlet weak var trimDeleteButton: UIButton!
    
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var makeTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    @IBOutlet weak var trimTextField: UITextField!
    var imagePicker: UIImagePickerController!
    
    var deleteButtons : [UIButton]!
    var buttons : [UIButton]!
    var selectedIndexButton = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configutationInterface()
        
        if UIScreen.main.bounds.width == 320 {
            cancelButton.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 17)!
        }
        
        removeInfo()
        
        let textFields = [yearTextField, makeTextField, modelTextField, trimTextField]
        let texts = ["Year", "Make", "Model", "Trim"]
        var i = 0
        for textField in textFields {
            let label = UILabel(frame: CGRect(x: 8, y: 2, width: 71, height: 40))
            label.text = texts[i]
            i += 1
            label.textColor = #colorLiteral(red: 0.5215686275, green: 0.5215686275, blue: 0.5215686275, alpha: 1)
            label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
            textField?.leftView = label
            textField?.leftViewMode = .always
            textField?.delegate = self
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        isValidToSend()
    }
    
    private func removeInfo() {
        deleteButtons =  [windshielDeleteButton, numberDeleteButton, vinNumberDeleteButton, trimDeleteButton]
        buttons = [windshieldButton, numberButton, vinNumberButton, trimButton]
        
        for button in deleteButtons {
            button.isHidden = true
        }
        
        for button in buttons {
            button.imageView?.contentMode = .scaleAspectFill
            button.imageView?.clipsToBounds = true
            button.setImage(UIImage(named: "placeholder_photo"), for: .normal)
        }
        
        let textFields = [yearTextField, makeTextField, modelTextField, trimTextField]
        for textField in textFields {
            textField?.text = ""
        }
    }
    
    
    func configutationInterface() {
        windshieldButton.layer.borderColor = #colorLiteral(red: 0.2039215686, green: 0.5098039216, blue: 0.9254901961, alpha: 1).cgColor
        numberButton.layer.borderColor = #colorLiteral(red: 0.2039215686, green: 0.5098039216, blue: 0.9254901961, alpha: 1).cgColor
        vinNumberButton.layer.borderColor = #colorLiteral(red: 0.2039215686, green: 0.5098039216, blue: 0.9254901961, alpha: 1).cgColor
        trimButton.layer.borderColor = #colorLiteral(red: 0.2039215686, green: 0.5098039216, blue: 0.9254901961, alpha: 1).cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.1803921569, green: 0.6196078431, blue: 0.7843137255, alpha: 1)
        isValidToSend()
        navigationItem.title = "get_picture".localize
        view.layoutIfNeeded()
    }

    func isValidToSend() {
        if  windshielDeleteButton.isHidden == true {
            setValidSend(false)
        }
        else if numberDeleteButton.isHidden == true {
            setValidSend(false)
        }
        else if vinNumberDeleteButton.isHidden == true {
            setValidSend(false)
        }
        else if trimDeleteButton.isHidden == true {
            setValidSend(false)
        }
        else if yearTextField.text?.count != 4 {
            setValidSend(false)
        }
        else if makeTextField.text?.count == 0 {
            setValidSend(false)
        }
        else if modelTextField.text?.count == 0  {
            setValidSend(false)
        }
        else if trimTextField.text?.count == 0 {
            setValidSend(false)
        }
        else {
            setValidSend(true)
        }
        
    }
    
    func setValidSend(_ isValid: Bool) {
        sendButton.isEnabled = isValid
        sendButton.backgroundColor = isValid ? #colorLiteral(red: 0.9725490196, green: 0.3803921569, blue: 0.1764705882, alpha: 1) : #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
    
    @IBAction func cancelTappped(_ sender: Any) {
        tabBarController?.selectedIndex = 0
    }
    
    @IBAction func getPictureTapped(_ sender: UIButton) {
        selectedIndexButton = sender.tag - 1
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            print("camera is not avaliable")
        }
    }
    
    @IBAction func deleteTapped(_ sender: UIButton) {
        let button = buttons[sender.tag - 1]
        button.setImage(UIImage(named: "placeholder_photo"), for: .normal)
        let deleteButton = deleteButtons[sender.tag - 1]
        deleteButton.isHidden = true
        isValidToSend()
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        let smtpSession = MCOSMTPSession()
        smtpSession.hostname = "smtp.gmail.com"
        smtpSession.username = "pic@rightauto.glass"
        smtpSession.password = "Radarjr42!"
        smtpSession.port     = 465
        smtpSession.authType = MCOAuthType.saslPlain
        smtpSession.connectionType = MCOConnectionType.TLS
        smtpSession.connectionLogger = {(connectionID, type, data) in
            if data != nil, let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                NSLog("Connectionlogger: \(string)")
            }
        }
        
        let builder = MCOMessageBuilder()
        builder.header.to = [MCOAddress(displayName: "", mailbox: "pic@rightauto.glass")]
        builder.header.from = MCOAddress(displayName: "", mailbox: "pic@rightauto.glass")
        builder.header.subject = DocusignManager.shared.email
        builder.textBody = "Year: \(yearTextField.text ?? "")\nMake: \(makeTextField.text ?? "")\nModel: \(modelTextField.text ?? "")\nTrim: \(trimTextField.text ?? "")"
        
        let windshieldDataImage = UIImageJPEGRepresentation(windshieldButton.imageView!.image!, 0.1)!
        let windshieldAttachment = MCOAttachment()
        windshieldAttachment.mimeType = "image/jpg"
        windshieldAttachment.filename = "windshield.jpg"
        windshieldAttachment.data = windshieldDataImage
        builder.addAttachment(windshieldAttachment)
        
        let numberDataImage = UIImageJPEGRepresentation(numberButton.imageView!.image!, 0.1)!
        let numberAttachment = MCOAttachment()
        numberAttachment.mimeType = "image/jpg"
        numberAttachment.filename = "part_number_on_windshield.jpg"
        numberAttachment.data = numberDataImage
        builder.addAttachment(numberAttachment)
        
        let vinNumberDataImage = UIImageJPEGRepresentation(vinNumberButton.imageView!.image!, 0.1)!
        let vinNumberAttachment = MCOAttachment()
        vinNumberAttachment.mimeType = "image/jpg"
        vinNumberAttachment.filename = "vin_number.jpg"
        vinNumberAttachment.data = vinNumberDataImage
        builder.addAttachment(vinNumberAttachment)
        
        let trimDataImage = UIImageJPEGRepresentation(trimButton.imageView!.image!, 0.1)!
        let trimAttachment = MCOAttachment()
        trimAttachment.mimeType =  "image/jpg"
        trimAttachment.filename = "trim_name_on_Vehicle.jpg"
        trimAttachment.data = trimDataImage
        builder.addAttachment(trimAttachment)
        
        let rfc822Data = builder.data()
        let sendOperation = smtpSession.sendOperation(with: rfc822Data!)
        SVProgressHUD.show(withStatus: "Sending".localize)
        sendOperation?.start { [weak self] (error) -> Void in
            SVProgressHUD.dismiss()
            if let error = error {
                NSLog("Error sending email: \(error)")
            } else {
                self?.removeInfo()
                self?.isValidToSend()
            }
        }
    }
    
}

extension GetPictureViewController: UIImagePickerControllerDelegate  {
    
    //MARK: - Add image to Library
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let button = buttons[selectedIndexButton]
        button.setImage(info[UIImagePickerControllerOriginalImage] as? UIImage, for: .normal)
        let deleteButton = deleteButtons[selectedIndexButton]
        deleteButton.isHidden = false
        isValidToSend()
    }
}

extension GetPictureViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            makeTextField.becomeFirstResponder()
        }
        if textField.tag == 2 {
            modelTextField.becomeFirstResponder()
        }
        if textField.tag == 3 {
            trimTextField.becomeFirstResponder()
        }
        if textField.tag == 4 {
            textField.resignFirstResponder()
        }
        isValidToSend()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if textField.tag != 1 {
            return true
        }
        let newLength = text.count + string.count - range.length
        return newLength <= 4
    }
}
