//
//  WorkOrdersViewController.swift
//  Right Auto Glass
//
//  Created by Katya on 3/23/18.
//

import UIKit
import SVProgressHUD
import Foundation
import DocuSignESign

protocol WorkOrdersViewControllerOutput: BaseViewType, DismissableType {
    var logout: (() -> Void)? { get set }
    var openWebControllerWithLink: ((String) -> Void)? { get set }
}

class WorkOrdersViewController: BaseViewController {
    
    // MARK: - Output
    let cellReuseIdentifier = "cell_template"
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var logoutButton: UIButton!
    @IBOutlet weak var noMessageLabel: UILabel!
    @IBOutlet weak var noIndoxView: UIView!
    @IBOutlet weak var headerView: UIView!
    
    private var refreshControl: UIRefreshControl!
    private var mTemplateList  = [DSFolderItem]()
    var logout: (() -> Void)?
    var openWebControllerWithLink: ((String) -> Void)?
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "TemplateTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellReuseIdentifier)
        tabBarController?.title = "Indox".localize
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "pull_to_refresh".localize)
        refreshControl.addTarget(self, action: #selector(updateTemplatesList), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationItem.title = "app_name".localize
        tabBarController?.tabBar.isHidden = false
        SVProgressHUD.show(withStatus: "loading".localize)
        updateTemplatesList()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: .UIApplicationDidBecomeActive,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIApplicationDidBecomeActive,
                                                  object: nil)
        
    }
    
    @objc func applicationDidBecomeActive() {
        SVProgressHUD.show(withStatus: "loading".localize)
        updateTemplatesList()
    }
    
    
    // MARK: - Private
    @objc private func updateTemplatesList() {
        noIndoxView.isHidden = true
        headerView.isHidden = mTemplateList.count == 0
        DocusignManager.shared.removeSignIn { (_) in }
        DocusignManager.shared.getFolderItemListWithCompletion { [weak self] (templateDefinitions) in
            if let templateDefinitions = templateDefinitions {
                self?.noIndoxView.isHidden = templateDefinitions.count != 0
                self?.headerView.isHidden = templateDefinitions.count == 0
                self?.mTemplateList = templateDefinitions   
                self?.tableView.reloadData()
            }
            self?.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
        }
    }
    
    // MARK: - Actions

    @IBAction func logOuttapped(_ sender: Any) {
        if let function = logout {
            function()
        }
    }
    
}

extension WorkOrdersViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Connectivity.isConnectedToInternet == false {
            showMessage("no_internet_connection_retry".localize)
            return
        }
        let template = mTemplateList[indexPath.row]
        let templateId: String = template.envelopeId
        SVProgressHUD.show(withStatus: "loading".localize)
        DocusignManager.shared.removeSignIn { (_) in
            DocusignManager.shared.displayTemplateForSignature(templateId, controller: self, url: nil) { (url, _) in
                SVProgressHUD.dismiss()
                if let url = url, let fun = self.openWebControllerWithLink {
                    fun(url)
                }
            }
        }
    }
}

extension WorkOrdersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mTemplateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TemplateTableViewCell
        let template = mTemplateList[indexPath.row]
        cell.setText(text: template.subject)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    private func displayErrorPrompt(errMsg: String) {
        let alert = UIAlertController(title: "error".localize, message: errMsg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok".localize, style: UIAlertActionStyle.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
