//
//  TabbarViewController.swift
//  Right Auto Glass
//
//  Created by Katya on 25.04.2018.
//

import UIKit
import Intercom

enum FlowType: Int {
    case getPhoto = 0
    case intercom = 1
}

protocol TabbarViewControllerOutput: BaseViewType {
    var onGetPictureSelect: ((UINavigationController) -> Void)? { get set }
    var onIntercomSelect:((UINavigationController) -> Void)? { get set }
    var onViewDidLoad: ((UINavigationController) -> Void)? { get set }
}

class TabbarViewController: UITabBarController, TabbarViewControllerOutput {

    var onGetPictureSelect: ((UINavigationController) -> Void)?
    var onIntercomSelect: ((UINavigationController) -> Void)?
    var onViewDidLoad: ((UINavigationController) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBar.tintColor = .white
        if let controller = customizableViewControllers?.first as? UINavigationController {
            onViewDidLoad?(controller)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4)) {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.updateUnreadCount),
                                                   name: NSNotification.Name.IntercomUnreadConversationCountDidChange,
                                                   object: nil)
            self.updateUnreadCount()
        }
    }
    
    func selectFlow(_ flowType: FlowType) {
        self.selectedIndex = flowType.rawValue
        guard let controller = viewControllers?[flowType.rawValue] as? UINavigationController else {
            return
        }
        tabBarController(self, didSelect: controller)
    }
    
    @objc func updateUnreadCount() {
        if Intercom.unreadConversationCount() == 0 {
            tabBar.items?[2].badgeValue = nil
        } else {
            tabBar.items?[2].badgeValue = String(Intercom.unreadConversationCount())
        }
    }
    
    deinit {
        print(String(describing: self))
    }
}

extension TabbarViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        guard let controller = viewControllers?[selectedIndex] as? UINavigationController else {
            return
        }
        
        if selectedIndex == 0 {
            onViewDidLoad?(controller)
        } else if selectedIndex == 1 {
            onGetPictureSelect?(controller)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController as? UINavigationController == nil {
            Intercom.presentConversationList()
            return false
        }
        return true
    }
}

