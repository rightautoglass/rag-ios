//
//  WorkOrdersCoordinator.swift
//  Right Auto Glass
//
//  Created by Katya on 25.04.2018.
//

import UIKit


protocol WorkOrdersCoordinatorOutput: Coordinator {
    
}

final class WorkOrdersCoordinator: BaseCoordinator, WorkOrdersCoordinatorOutput {
    // MARK: - Output
    
    // MARK: - Init injection
    private let factory: WorkOrdersModuleFactoryType
    private let router: RouterType
    
    // MARK: - Lifecycle
    init(factory: WorkOrdersModuleFactoryType, router: RouterType) {
        self.factory = factory
        self.router = router
    }
    
    override func start() {
    }
}
