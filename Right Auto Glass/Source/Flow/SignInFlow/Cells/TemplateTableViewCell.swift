//
//  TemplateTableViewCell.swift
//  docusign-sdk-sample-swift
//
//  
//  Copyright © 2017 DocuSign. All rights reserved.
//

import UIKit

class TemplateTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var lblTemplateName: UILabel!
    var downloadBlock : (() -> Void)?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setText(text: String) {
        lblTemplateName.text = text
    }
    
}
