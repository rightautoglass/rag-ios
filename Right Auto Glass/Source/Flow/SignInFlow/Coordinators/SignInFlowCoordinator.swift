import Foundation

protocol SignInCoordinatorOutput: Coordinator {
  var finishFlow: (() -> Void)? { get set }
}

final class SignInFlowCoordinator: BaseCoordinator, SignInCoordinatorOutput {
  // MARK: - Output
  var finishFlow: (() -> Void)?

  // MARK: - Property injection
  var signInFlowApiManager: SignInFlowManagerType!

  // MARK: - Init injection
  private let factory: SignInFlowModuleFactoryType
  private let router: RouterType

  // MARK: - Lifecycle
  init(factory: SignInFlowModuleFactoryType, router: RouterType) {
    self.factory = factory
    self.router = router
  }

  override func start() {
    showsSigninScreen()
  }
}

extension SignInFlowCoordinator {
  private func showsSigninScreen() {
    let signInFlowScreenOutput = factory.makeSignInFlowScreenOutput()
    signInFlowScreenOutput.onNextScreenButtonPressed = { [weak self] in
        self?.finishFlow!()
    }
    signInFlowScreenOutput.onForgotPasswordButtonPressed = { [weak self] in
        self?.showForgotScreen()
    }
    router.setRootModule(signInFlowScreenOutput)
  }

  private func showForgotScreen() {
    let vc = factory.makeForgotPasswordScreenOutput()
    vc.title = "forgot_password".localize
    router.push(vc, animated: true, completion: nil)
  }

}

extension SignInFlowCoordinator {
  private func getToken(success: @escaping (String) -> Void, failure: @escaping () -> Void) {
      let successHandler: (String) -> Void = { token in
        success(token)
      }
      let failureHandler: () -> Void = {
        failure()
      }
      signInFlowApiManager.getToken(success: successHandler, failure: failureHandler)
  }
}
