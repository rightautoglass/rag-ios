//
//  SignUpViewController.swift
//  Right Auto Glass
//
//  Created by Katya on 3/23/18.
//
import SVProgressHUD
import SwiftKeychainWrapper

protocol SignUpViewControllerOutput: BaseViewType, DismissableType {
    var onNextScreenButtonPressed: (() -> Void)? { get set }
    var onForgotPasswordButtonPressed: (() -> Void)? { get set }
}

class SignInViewController: BaseViewController, SignUpViewControllerOutput {
    // MARK: - Output
    var onNextScreenButtonPressed: (() -> Void)?
    var onForgotPasswordButtonPressed: (() -> Void)?
    var onDismiss: (() -> Void)?
    var onSelectDate: ((@escaping (String) -> Void) -> Void)?
    var onDateSelected: ((String) -> Void)?
    
    @IBOutlet weak private var password: UITextField!
    @IBOutlet weak private var email: UITextField!
    @IBOutlet weak private var forgotButton: UIButton!
    @IBOutlet weak private var loginButton: UIButton!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    private func setupInterface() {
        email.placeholder = "email".localize
        password.placeholder = "password".localize
        loginButton.setTitle("login".localize, for: .normal)
        forgotButton.setTitle("forgot_password".localize, for: .normal)
        
        #if DEBUG
        email.text = "vladshul2009@gmail.com"
        password.text = "!1945RAKatya!"
        #endif
    }
    
    // MARK: - Actions
    
    @IBAction func loginTapped(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet == false {
            showMessage("no_internet_connection_retry".localize)
            return
        }
        
        if let email = email.text, let password = password.text {
            if  email.isValidEmail() == false || email.count == 0 {
                showMessage("please_enter_a_valid_email_address".localize)
                return
            } else if password.isEmpty {
                showMessage("please_enter_a_valid_password".localize)
                return
            }
            SVProgressHUD.show(withStatus: "Authentication...")
            DocusignManager.shared.login(email, password: password) { [weak self] (isSuccess) in
                SVProgressHUD.dismiss()
                if isSuccess == true, let function = self?.onNextScreenButtonPressed {
                    function()
                } else {
                    self?.showMessage("incorrect_email_or_password".localize)
                }
            }
        } else {
            showMessage("incorrect_email_or_password".localize)
        }
    }
    
    @IBAction func forgotButtonTapped(_ sender: Any) {
        if let fun = onForgotPasswordButtonPressed {
            fun()
        }
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            password.becomeFirstResponder()
        } else if textField.tag == 2 {
            password.resignFirstResponder()
        }
        return true
    }
}
