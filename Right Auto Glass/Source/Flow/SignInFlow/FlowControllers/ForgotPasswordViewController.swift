//
//  ForgotPasswordViewController.swift
//  Right Auto Glass
//
//  Created by Katya on 3/23/18.
//

import UIKit
import WebKit
import SVProgressHUD

class ForgotPasswordViewController: BaseViewController {

    var webView: WKWebView!
    var webUrl = "https://account.docusign.com/forgotpassword"

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInterface()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.dismiss()
    }

    private func setupInterface() {
        
        navigationController?.navigationBar.isHidden = false
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = false
        
        let url = URL(string: webUrl)
        let request = URLRequest(url: url!)
        
        
        let userContentController = WKUserContentController()
        if let cookies = HTTPCookieStorage.shared.cookies {
            let script = getJSCookiesString(for: cookies)
            let cookieScript = WKUserScript(source: script, injectionTime: .atDocumentStart, forMainFrameOnly: false)
            userContentController.addUserScript(cookieScript)
        }
        let webViewConfig = WKWebViewConfiguration()
        webViewConfig.userContentController = userContentController

        webView =  WKWebView(frame: view.frame, configuration: webViewConfig)
        webView.navigationDelegate = self
        webView.load(request)
        view.addSubview(webView)
        view.sendSubview(toBack: webView)
    }
    
    ///Generates script to create given cookies
    public func getJSCookiesString(for cookies: [HTTPCookie]) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss zzz"
        
        for cookie in cookies {
            result += "document.cookie='\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path); "
            if let date = cookie.expiresDate {
                result += "expires=\(dateFormatter.string(from: date)); "
            }
            if (cookie.isSecure) {
                result += "secure; "
            }
            result += "'; "
        }
        return result
    }
}

extension ForgotPasswordViewController: WKNavigationDelegate {
    /* Start the network activity indicator when the web view is loading */
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation) {
        SVProgressHUD.show()
    }
    
    /* Stop the network activity indicator when the loading finishes */
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation) {
        SVProgressHUD.dismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            webView.scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
        }
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()",
                                   completionHandler: { [weak self](html: Any?, error: Error?) in
                                    if let html = html as? String {
                                        if html.contains("na3.docusign.net"), html.contains("na2.docusign.net"), html.contains("eu1.docusign.net") {
                                            self?.navigationController?.popViewController(animated: true)
                                        }
                                    }
        })
    }

    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        showMessage(error.localizedDescription)
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
        showMessage(error.localizedDescription)
    }

}
