import Foundation

protocol ComponentFactoryType: class {
  func makeSignInFlowManager() -> SignInFlowManagerType
}

final class ComponentFactory: ComponentFactoryType {
  func makeSignInFlowManager() -> SignInFlowManagerType {
    return APIManager()
  }
}
