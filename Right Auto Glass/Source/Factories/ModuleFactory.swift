import UIKit

protocol SignInFlowModuleFactoryType {
    func makeSignInFlowScreenOutput() -> SignUpViewControllerOutput
    func makeForgotPasswordScreenOutput() -> ForgotPasswordViewController
}

protocol GetPictureModuleFactoryType {
     func makeGetPictureScreenOutput() -> GetPictureViewController
}

protocol WorkOrdersModuleFactoryType {
    func makeWorkOrdersScreenOutput() -> WorkOrdersViewController
}


final class ModuleFactory {
    let component: ComponentFactoryType
    
    init(component: ComponentFactoryType) {
        self.component = component
    }
}

extension ModuleFactory: SignInFlowModuleFactoryType {
    func makeSignInFlowScreenOutput() -> SignUpViewControllerOutput {
        return UIStoryboard(.signIn).instantiateController() as SignInViewController
    }
    
    func makeForgotPasswordScreenOutput() -> ForgotPasswordViewController {
        return UIStoryboard(.signIn).instantiateViewController(withIdentifier: String(describing: ForgotPasswordViewController.self)) as! ForgotPasswordViewController
    }
    
}

extension ModuleFactory: GetPictureModuleFactoryType {
    
    func makeGetPictureScreenOutput() -> GetPictureViewController {
        return UIStoryboard(.getPicture).instantiateController() as GetPictureViewController
    }
    
}

extension ModuleFactory: WorkOrdersModuleFactoryType {
    
    func makeWorkOrdersScreenOutput() -> WorkOrdersViewController {
        return UIStoryboard(.main).instantiateController() as WorkOrdersViewController
    }
    
}
