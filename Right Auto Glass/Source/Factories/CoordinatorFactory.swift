import UIKit

protocol CoordinatorSubFactories {
    var moduleFactory: ModuleFactory { get }
    var component: ComponentFactoryType { get }
}

protocol CoordinatorFactoryType {
    func makeTabBarOutput() -> TabbarViewControllerOutput
    func makeApplicationCoordinator(window: UIWindow) -> ApplicationCoordinator
    func makeMainOutputCoordinator(_ navigationController: UINavigationController?) -> WorkOrdersCoordinatorOutput
    func makeLoadOutput() -> UIViewController
    func makeSignInFlowCoordinator() -> (SignInCoordinatorOutput, Presentable)
    func makeWebOutput() -> UIViewController
    func makeGetPictureCoordinator(_ navigationController: UINavigationController?) -> GetPictureCoordinatorOutput
}

final class CoordinatorFactory: CoordinatorSubFactories {
    private(set) var moduleFactory: ModuleFactory
    private(set) var component: ComponentFactoryType
    
    init() {
        self.component = ComponentFactory()
        self.moduleFactory = ModuleFactory(component: self.component)
    }
}

extension CoordinatorFactory: CoordinatorFactoryType {
    
    func makeTabBarOutput() -> TabbarViewControllerOutput {
        return UIStoryboard(.main).instantiateController() as TabbarViewController
    }
    
    func makeGetPictureCoordinator(_ navigationController: UINavigationController?) -> GetPictureCoordinatorOutput {
        let coordinator = GetPictureCoordinator(factory: moduleFactory, router: makeRouter(navigationController))
        return coordinator
    }
    
    func makeMainOutputCoordinator(_ navigationController: UINavigationController?) -> WorkOrdersCoordinatorOutput {
        let coordinator = WorkOrdersCoordinator(factory: moduleFactory, router: makeRouter(navigationController))
        return coordinator
    }
    
    func makeApplicationCoordinator(window: UIWindow) -> ApplicationCoordinator {
        let coordinator = ApplicationCoordinator(coordinatorFactory: self, mainWindow: window)
        return coordinator
    }
    
    func makeLoadOutput() -> UIViewController {
        return UIStoryboard(.load).instantiateController() as UIViewController
    }
    
    func makeMainOutput() -> UINavigationController {
        return UIStoryboard(.main).instantiateController() as UINavigationController
    }
    
    func makeSignInFlowCoordinator() -> (SignInCoordinatorOutput, Presentable) {
        let router = makeRouter(nil)
        let coordinator = SignInFlowCoordinator(factory: moduleFactory, router: router)
        coordinator.signInFlowApiManager = component.makeSignInFlowManager()
        return (coordinator, router)
    }
    
    
    func makeWebOutput()  -> UIViewController {
        return UIStoryboard(.signIn).instantiateViewController(withIdentifier: String(describing: ForgotPasswordViewController.self)) as! ForgotPasswordViewController
    }
    
}

extension CoordinatorFactory {
    private func makeRouter(_ navigationController: UINavigationController?) -> RouterType {
        return Router(rootController: makeNavigation(navigationController))
    }
    
    private func makeNavigation(_ navigationController: UINavigationController?) -> UINavigationController {
        guard let navigationController = navigationController else {
            let storyboard = UIStoryboard(.main)
            return storyboard.instantiateController()
        }
        
        return navigationController
    }
}
