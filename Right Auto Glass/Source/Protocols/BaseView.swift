import UIKit

protocol BaseViewType: class, Presentable {}

protocol DismissableType: class {
  var onDismiss: (() -> Void)? { get set }
}
