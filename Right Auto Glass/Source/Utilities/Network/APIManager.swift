import Foundation
import Moya

protocol APIManagerPrivateType {
  var provider: MoyaProvider<APIProvider> { get set }
}

final class APIManager: APIManagerPrivateType {
  lazy var provider: MoyaProvider<APIProvider> = { return MoyaProvider<APIProvider>() }()
}
