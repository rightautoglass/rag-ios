import Foundation
import Moya

struct ServerConfig {
  static let baseUrl = "https://your.project.base.url/"
}

enum APIProvider {
    case getToken
    case sendDeviceToken
}

extension APIProvider: TargetType {
  var baseURL: URL { return URL(string: ServerConfig.baseUrl + "api")! }
  
  var path: String {
    switch self {
    case .getToken:
      return "/getToken"
    case .sendDeviceToken:
      return "/sendToken"
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .getToken:
      return .get
    case .sendDeviceToken:
        return .post
    }
  }
  
  var task: Task {
    switch self {
    case .getToken:
      return .requestPlain
    case .sendDeviceToken:
        return .requestPlain
    }
  }
  
  var sampleData: Data {
    return Data()
  }
  
  var headers: [String: String]? {
    var headers: [String: String] = ["Content-type": "application/json"]
    headers["X-Requested-With"] = "XMLHttpRequest"
    headers["Device-Type"] = "iOS"
    headers["Device-Id"] = "deviceId"
    headers["Authorization"] = "token"
    return headers
  }
}
