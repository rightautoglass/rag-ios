import Foundation
import Moya

protocol SignInFlowManagerType {
  func getToken(success: @escaping (String) -> Void, failure: @escaping () -> Void)
}

extension APIManager: SignInFlowManagerType {
  func getToken(success: @escaping (String) -> Void, failure: @escaping () -> Void) {
    provider.request(.getToken) { (response) in
      switch response {
      case .success(let response):
        do {
          let successfullResponse = try response.filterSuccessfulStatusCodes()
          _ = try successfullResponse.mapJSON()
          DispatchQueue.main.async {
            success("token")
          }
        } catch {
          DispatchQueue.main.async {
            failure()
          }
        }
      case .failure:
        DispatchQueue.main.async {
          failure()
        }
      }
    }
  }
    
    func sendDeviceToken(success: @escaping (String) -> Void, failure: @escaping () -> Void) {
        provider.request(.getToken) { (response) in
            switch response {
            case .success(let response):
                do {
                    let successfullResponse = try response.filterSuccessfulStatusCodes()
                    _ = try successfullResponse.mapJSON()
                    DispatchQueue.main.async {
                        success("token")
                    }
                } catch {
                    DispatchQueue.main.async {
                        failure()
                    }
                }
            case .failure:
                DispatchQueue.main.async {
                    failure()
                }
            }
        }
    }
    
    
 }
