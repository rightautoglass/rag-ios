import UIKit
import Intercom
import SwiftKeychainWrapper

final class ApplicationCoordinator: BaseCoordinator {
    // MARK: - Init injection
    private let coordinatorFactory: (CoordinatorFactoryType & CoordinatorSubFactories)
    private let window: UIWindow
    
    // MARK: - Life cycle properties
    private var tabBarController: TabbarViewController?
    
    // MARK: - Life cycle
    init(coordinatorFactory: (CoordinatorFactoryType & CoordinatorSubFactories),
         mainWindow: UIWindow) {
        self.coordinatorFactory = coordinatorFactory
        window = mainWindow
        
        super.init()
    }
    
    override func start() {
        if let password = KeychainWrapper.standard.string(forKey: AppConstant.passwordKey),
            let email = KeychainWrapper.standard.string(forKey: AppConstant.emailKey) {
            runLoadFlow()
            DocusignManager.shared.login(email, password: password) { [weak self] (isAuthorized) in
                if isAuthorized {
                    self?.window.rootViewController = self?.runTabBarFlow().toPresent
                } else {
                    self?.runSignInFlow()
                }
            }
        } else {
            runSignInFlow()
        }
    }
}

// MARK: - Private
extension ApplicationCoordinator {
    
    private func runTabBarFlow() -> TabbarViewControllerOutput {
        let tabbarFlow = coordinatorFactory.makeTabBarOutput()
        tabbarFlow.onViewDidLoad = runMainFlow()
        tabbarFlow.onGetPictureSelect = runGetPictureFlow()
        
        tabBarController = tabbarFlow as? TabbarViewController
        window.rootViewController = tabBarController
        return tabbarFlow
    }
    
    private func runMainFlow() -> ((UINavigationController) -> Void) {
        return { [unowned self] navigationController in
            if navigationController.viewControllers.isEmpty == true {
                let mainCoordinator = self.coordinatorFactory.makeMainOutputCoordinator(navigationController)
                mainCoordinator.start()
                self.addDependency(mainCoordinator)
            }
            let viewController = navigationController.viewControllers.first as! WorkOrdersViewController
            viewController.openWebControllerWithLink = { [weak self] (url) in
                self?.showWebView(url, nc: navigationController )
            }

            viewController.logout = { [weak self] in
                DocusignManager.shared.logout()
                self?.runSignInFlow()
            }
        }
    }
    
    private func runGetPictureFlow() -> ((UINavigationController) -> Void) {
        return { [unowned self] navigationController in
            if navigationController.viewControllers.isEmpty == true {
                let getPictureCoordinator = self.coordinatorFactory.makeGetPictureCoordinator(navigationController)
                getPictureCoordinator.start()
                self.addDependency(getPictureCoordinator)
            }
        }
    }
    
    private func showWebView(_ link: String, nc: UINavigationController) {
        let vc = UIStoryboard(.signIn).instantiateViewController(withIdentifier: String(describing: ForgotPasswordViewController.self)) as! ForgotPasswordViewController
        vc.webUrl = link
        vc.title = ""
        nc.pushViewController(vc, animated: true)
    }

    private func runLoadFlow() {
        let viewControlle = coordinatorFactory.makeLoadOutput()
        window.rootViewController = viewControlle
    }
    
    private func openIntercome() {
        Intercom.presentMessenger()
    }
    
    private func runSignInFlow() {
        let (signInFlowCoordinator, router) = coordinatorFactory.makeSignInFlowCoordinator()
        signInFlowCoordinator.finishFlow = { [weak self, weak signInFlowCoordinator] in
            guard let strongSelf = self else { return }
            strongSelf.removeDependency(signInFlowCoordinator)
            strongSelf.window.rootViewController = strongSelf.runTabBarFlow().toPresent
        }
        removeAllDependencies()
        signInFlowCoordinator.start()
        addDependency(signInFlowCoordinator)
        window.rootViewController = router.toPresent
    }
    
}
