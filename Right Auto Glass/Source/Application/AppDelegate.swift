import UIKit
import Fabric
import Crashlytics
import Intercom
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    // MARK: - Lifecycle properties
    var window: UIWindow?
    
    // MARK: - Private properties
    private let coordinatorFactory = CoordinatorFactory()
    private lazy var applicationCoordinator: ApplicationCoordinator = self.makeCoordinator()()
    
    // MARK: - Lifecycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureIntercome()
        configureFabric()
        configureWindow()
        registerForPushNotifications()
        configureInterface()
        return true
    }
    
    // MARK: - Private
    private func configureWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        applicationCoordinator.start()
        window?.makeKeyAndVisible()
    }
    
    private func configureInterface() {
        UINavigationBar.appearance().backgroundColor = #colorLiteral(red: 0, green: 0.5490196078, blue: 0.7568627451, alpha: 1)
        UIBarButtonItem.appearance().tintColor = .white
        UITabBar.appearance().unselectedItemTintColor = #colorLiteral(red: 0.4078431373, green: 0.7917170904, blue: 0.8666666667, alpha: 1)
        let font = UIFont(name: "AppleSDGothicNeo-Bold", size: UIScreen.main.bounds.width == 320 ? 17: 19)!
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: font]
        UINavigationBar.appearance().tintColor = .white
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateUnreadCount),
                                               name: NSNotification.Name.IntercomUnreadConversationCountDidChange,
                                               object: nil)
        
    }
    
    private func configureIntercome() {
        Intercom.setApiKey(AppConstant.inpercomApiKey, forAppId: AppConstant.intercomeApiId)
        Intercom.setInAppMessagesVisible(false)
    }
    
    private func configureFabric() {
        Fabric.with([Crashlytics.self])
    }
    
    private func makeCoordinator() -> (() -> ApplicationCoordinator) {
        return { [unowned self] () -> ApplicationCoordinator in
            return self.coordinatorFactory.makeApplicationCoordinator(window: self.window!)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Intercom.setDeviceToken(deviceToken)
        DocusignManager.shared.deviceToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        
//        let smtpSession = MCOSMTPSession()
//        smtpSession.hostname = "smtp.gmail.com"
//        smtpSession.username = "pic@rightauto.glass"
//        smtpSession.password = "Radarjr42!"
//        smtpSession.port     = 465
//        smtpSession.authType = MCOAuthType.saslPlain
//        smtpSession.connectionType = MCOConnectionType.TLS
//        smtpSession.connectionLogger = {(connectionID, type, data) in
//        }
//
//        let builder = MCOMessageBuilder()
//        builder.header.to = [MCOAddress(displayName: "", mailbox: "katya_o@i.ua")]
//        builder.header.from = MCOAddress(displayName: "", mailbox: "pic@rightauto.glass")
//        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        print(token)
//        builder.header.subject = token
//
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        // Print it to console
//        print("APNs device token: \(deviceTokenString)")
//
//         builder.textBody = "token \(deviceTokenString)"
//
//        let rfc822Data = builder.data()
//        let sendOperation = smtpSession.sendOperation(with: rfc822Data!)
//        sendOperation?.start {(error) -> Void in
//            if let error = error {
//                NSLog("Error sending email: \(error)")
//            } else {
//            }
//        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, _) in
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                 UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = Int(Intercom.unreadConversationCount()) + DocusignManager.shared.indoxCount
    }
    
    @objc func updateUnreadCount() {
         UIApplication.shared.applicationIconBadgeNumber = Int(Intercom.unreadConversationCount()) + DocusignManager.shared.indoxCount
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Intercom.isIntercomPushNotification(userInfo) {
            Intercom.handlePushNotification(userInfo)
        }
        completionHandler(.noData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("\(userInfo)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("\(String(describing: userInfo))")
    }
    
    func showAlertAppDelegate( title : String, message : String, buttonTitle : String, window: UIWindow) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}
