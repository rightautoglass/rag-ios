//
//  AppConstant.swift
//  Right Auto Glass
//
//  Created by Katya on 29.03.2018.
//

import UIKit

class AppConstant: NSObject {
    
    //intercom
    static let inpercomApiKey = "ios_sdk-8ed0b5ba9a38ccbe9958697e77a1e785df732aa3"
    static let intercomeApiId = "oy4bejay"
    
    //docusign
    static let docusignKey = "0f21b612-0ccf-47fa-a94c-365476c87f58" 
    static let docusignHost = URL(string: "https://eu1.docusign.net/restapi/")
    
    static let  firstNameKey = "firstName"
    static let  lastNameKey  = "lastName"
    static let  addressKey   = "address"
    static let  cityKey      = "city"
    static let  stateKey     = "state"
    static let  countryKey   = "country"
    static let  zipCodeKey   = "zipCode"
    static let  phoneKey     = "phone"

    static let passwordKey   = "password"
    static let emailKey      = "email"

}
