import Foundation

extension String {
  var localize: String {
    return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: self, comment: "")
  }
    
  func isValidEmail() -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    
    return emailTest.evaluate(with: self)
 }
    
}
