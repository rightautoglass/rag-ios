import UIKit

enum Story: String {
  case main   = "Main"
  case signIn = "SignInFlow"
  case load   = "Load"
  case getPicture = "GetPicture"
}

protocol Reusable {
  static var reuseIdentifier: String { get }
}

extension Reusable {
  static var reuseIdentifier: String {
    return String(describing: Self.self)
  }
}

protocol InterfaceBuilderPrototypable {
  static var nib: UINib { get }
}

extension InterfaceBuilderPrototypable {
  static var nib: UINib {
    return UINib(nibName: String(describing: Self.self), bundle: nil)
  }
}

extension UICollectionView {
  
  func register<T: UICollectionViewCell>(_ : T.Type) where T: Reusable {
    register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
  }
  
  func register<T: UICollectionViewCell>(_ : T.Type) where T: Reusable, T: InterfaceBuilderPrototypable {
    register(T.nib, forCellWithReuseIdentifier: T.reuseIdentifier)
  }
  
  func dequeue<T: UICollectionViewCell>(_: T.Type, for indexPath: IndexPath) -> T where T: Reusable {
    guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
      abort()
    }
    
    return cell
  }
}

extension UITableView {
  // MARK: - UITableViewCell
  func register<T: UITableViewCell>(_ : T.Type) where T: Reusable {
    register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
  }

  func register<T: UITableViewCell>(_ : T.Type) where T: Reusable, T: InterfaceBuilderPrototypable {
    register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
  }

  func dequeue<T: UITableViewCell>(_ : T.Type, for indexPath: IndexPath) -> T where T: Reusable {
    guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
      abort()
    }
    return cell
  }

  // MARK: - UITableViewHeaderFooterView
  func register<T: UITableViewHeaderFooterView>(_ : T.Type) where T: Reusable, T: InterfaceBuilderPrototypable {
    register(T.nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
  }

  func dequeue<T: UITableViewHeaderFooterView>(_ : T.Type) -> T where T: Reusable {
    guard let header = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
      abort()
    }
    return header
  }
}

protocol StoryboardIdentifiable {
  static var identifier: String { get }
}

protocol Presentable {
  var toPresent: UIViewController? { get }
}

extension UIStoryboard {
  convenience init(_ story: Story) {
    self.init(name: story.rawValue, bundle: nil)
  }
  
  func instantiateController<T: UIViewController>() -> T {
    guard let viewController = self.instantiateViewController(withIdentifier: T.identifier) as? T else {
      fatalError("Could not load view controller with identifier\(T.identifier)")
    }
    return viewController
  }
}

extension UIViewController: StoryboardIdentifiable {}
extension UIViewController: Presentable {
  var toPresent: UIViewController? {
    return self
  }
}

extension UIViewController {
  func hideKeyboardIfNeeded() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }

  @objc private func dismissKeyboard() {
    view.endEditing(true)
  }

  func pushAnimation() {
    let transition = CATransition()
    transition.duration = 0.5
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromRight
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    self.view.window?.layer.add(transition, forKey: kCATransition)
  }

  func popAnimation() {
    let transition = CATransition()
    transition.duration = 0.5
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromLeft
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    self.view.window?.layer.add(transition, forKey: kCATransition)
  }

  func presentAsPush(_ module: Presentable?) {
    guard let controller = module?.toPresent else { return }
    self.pushAnimation()
    self.present(controller, animated: false, completion: nil)
  }

  func dismissAsPop() {
    self.popAnimation()
    self.dismiss(animated: false, completion: nil)
  }
}

extension StoryboardIdentifiable where Self: UIViewController {
  static var identifier: String {
    return String(describing: self)
  }
}
