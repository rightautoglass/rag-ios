import UIKit
import Alamofire
import SVProgressHUD

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet: Bool {
        return self.sharedInstance.isReachable
    }
}

class BaseViewController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    let reachabilityManager = NetworkReachabilityManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setDefaultMaskType(.clear)
        startNetworkReachabilityObserver()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func startNetworkReachabilityObserver() {
        reachabilityManager?.listener = { [weak self ] status in
            
            switch status {
                
            case .notReachable:
                SVProgressHUD.dismiss()
                self?.showMessage("no_internet_connection_retry".localize)
            case .unknown :
                 self?.showMessage("no_internet_connection_retry".localize)
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
                
            }
        }
        reachabilityManager?.startListening()
    }
    
    
  deinit {
    print(String(describing: self))
  }

  override var prefersStatusBarHidden: Bool {
    return false
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
    
  func promptError(err: Error?) {
    let title = "error".localize
    let message = err?.localizedDescription
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "ok".localize, style: .default) { _ in })
    
    self.present(alert, animated: true)
  }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localize, style: .default) { _ in })
        present(alert, animated: true)
    }
    

}
