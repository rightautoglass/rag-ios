class BaseCoordinator: Coordinator {
  private var childCoordinators: [Coordinator] = []
  
  func start() {
    assertionFailure("must be overriden")
  }
  
  func addDependency(_ coordinator: Coordinator) {
    for element in childCoordinators where element === coordinator {
      return
    }
    
    childCoordinators.append(coordinator)
  }
  
  func removeDependency(_ coordinator: Coordinator?) {
    guard childCoordinators.isEmpty == false, let coordinator = coordinator else { return }
    
    for (index, element) in childCoordinators.enumerated() where element === coordinator {
      childCoordinators.remove(at: index)
      break
    }
  }
  
  func removeAllDependencies() {
    print("Must by removed: \(childCoordinators)")
    for coord in childCoordinators {
      removeDependency(coord)
    }
  }
  
  deinit {
    print(String(describing: self))
  }
  
  func dependencies() -> [Coordinator] {
    return childCoordinators
  }
}
