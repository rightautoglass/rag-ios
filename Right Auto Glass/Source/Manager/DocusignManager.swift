//
//  ProfileManager.swift
//  docusign-sdk-sample-swift
//
//  
//  Copyright © 2017 DocuSign. All rights reserved.
//

import Intercom
import DocuSignESign
import SwiftKeychainWrapper

class DocusignManager {
    
    static let shared = DocusignManager()

    var accountID = ""
    var baseUrl   = AppConstant.docusignHost
    var email = ""
    var password = ""
    var folderID = ""
    var userName = ""
    var userId = ""
    var signatureId = ""
    var indoxCount = 0
    var deviceToken : String? {
        didSet {
           updateUserPushToken()
        }
    }
    
    private let headerName = "X-DocuSign-Authentication"
    private var clientData = [String: String]()
    private var folderName = "Inbox"
    
    
    // keys for custom fields on template
    static let tabKeyName             = "Text dc02e5f6-ff92-4d84-b13d-d82f4c886e81"
    static let tabKeyAddress1         = "Text 05f929aa-b3c6-4c17-bf22-3f7e6bfbe473"
    static let tabKeyAddress2         = "Text 526bf465-583e-426f-9671-04dadbb16dbc"
    static let tabKeyAddress3         = "Text fa6374f6-1fe0-4a71-907a-0c3342791df3"
    static let tabKeyClientNumber     = "Text c8985ea9-4126-4c70-832f-b8f06c76fae1"
    static let tabKeyInvestmentAmount = "Text 9edc0b80-1604-4079-bf0a-1b0b2a5be361"
    
    //This prevents others from using the default '()' initializer for this class.
    private init() {

    }
    
    func getHeaderJsonString() -> String {
        let headerDict = ["Username": email, "Password": password, "IntegratorKey": AppConstant.docusignKey]
        let jsonData = try? JSONSerialization.data(withJSONObject: headerDict, options: [])
        return String(data: jsonData!, encoding: .utf8) ?? ""
    }
    
    /**
     Returns Dictionary of the data that will be passed into the template
     */
    func getTemplateTabData() -> [String: String] {
        return clientData
    }
    
    func login(_ email: String, password: String, completion: @escaping ((Bool) -> Void)) {
        self.email = email
        self.password = password
        
        let apiClient = DSApiClient(baseURL: AppConstant.docusignHost)
        let jsonString = getHeaderJsonString()
        let loginOptioons = DSAuthenticationApi_LoginOptions()
        loginOptioons.loginSettings = "none"
        loginOptioons.apiPassword   = "true"
        loginOptioons.includeAccountIdGuid = "true"
        
        let authApi = DSAuthenticationApi(apiClient: apiClient)
        authApi?.setDefaultHeaderValue(jsonString, forKey: headerName)
        Intercom.logout()
        _ = authApi?.login(withApiPassword: loginOptioons, completionHandler: { (loginInformation, error) in
            if let account = loginInformation?.loginAccounts.first as? DSLoginAccount {
                self.accountID = account.accountId
                self.baseUrl = URL(string: account.baseUrl)
                self.userName = account.name
                self.userId = account.userId
                let mas = account.baseUrl.components(separatedBy: "/v2")
                if let url = mas.first {
                    self.baseUrl = URL(string: url)
                }
                Intercom.registerUser(withEmail: email)
                KeychainWrapper.standard.set(password, forKey: AppConstant.passwordKey)
                KeychainWrapper.standard.set(email, forKey: AppConstant.emailKey)
                self.updateUserPushToken()
                completion(error == nil)
            } else {
                completion(false)
            }
        })
    }
    
    func updateUserPushToken() {
        if userId != "" && userId != "" {
            let apiClient = DSApiClient(baseURL:baseUrl)
            let jsonString = getHeaderJsonString()
            let userApi = DSUsersApi(apiClient: apiClient)
            userApi?.setDefaultHeaderValue(jsonString, forKey: headerName)
            
            let list = DSUserProfile()

            let address = DSAddressInformationV2()
            address.address2 = deviceToken
            list.address = address
            
            _ = userApi?.updateProfile(withAccountId: accountID, userId: userId, userProfile: list, completionHandler: { (error) in
                print(error ?? "")
            })

        }
    }
    
    func logout() {
        KeychainWrapper.standard.removeObject(forKey: AppConstant.passwordKey)
        KeychainWrapper.standard.removeObject(forKey: AppConstant.emailKey)
        Intercom.registerUnidentifiedUser()
        Intercom.setLauncherVisible(false)
    }
    
    typealias templateCompletionHandler = (_ templateDefinitions: [DSFolderItem]?) -> Void
    func getFolderItemListWithCompletion(completionHandler: @escaping templateCompletionHandler) {
        let jsonString = getHeaderJsonString()
        let apiClient = DSApiClient(baseURL: baseUrl)
        let folderApi = DSFoldersApi(apiClient: apiClient)
        folderApi?.setDefaultHeaderValue(jsonString, forKey: headerName)
        let options = DSFoldersApi_ListOptions()
        _ = folderApi?.list(withAccountId: accountID, options: options, completionHandler: { (folders, error) in
            if let folderList = folders?.folders as? [DSFolder], error == nil {
                let filteredFolder = folderList.filter { $0.name == self.folderName }
                self.folderID = filteredFolder.first?.folderId ?? "0"
                self.getFolderItemList(completionHandler: completionHandler)
            } else {
                completionHandler([])
            }
        })
    }
    
    func removeSignIn(completionHandler: @escaping ((Bool) -> Void)) {
        let jsonString = getHeaderJsonString()
        let apiClient = DSApiClient(baseURL:baseUrl)
        let userApi = DSUsersApi(apiClient: apiClient)
        userApi?.setDefaultHeaderValue(jsonString, forKey: headerName)
       let option = DSUsersApi_ListSignaturesOptions()
       
        _ = userApi?.listSignatures(withAccountId: accountID, userId: userId, options: option, completionHandler: { (info, error) in
            if let signs = info?.userSignatures as? [DSUserSignature] {
                for sign in signs {
                    _ = userApi?.deleteSignature(withAccountId: self.accountID, userId: self.userId, signatureId: sign.signatureId, completionHandler: { (error) in
                        print(error ?? "")
                    })
                }
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        })
    }
    
    func getFolderItemList(completionHandler: @escaping templateCompletionHandler) {
        let jsonString = getHeaderJsonString()
        let apiClient = DSApiClient(baseURL: baseUrl)
        let folderApi = DSFoldersApi(apiClient: apiClient)
        folderApi?.setDefaultHeaderValue(jsonString, forKey: headerName)
        let options = DSFoldersApi_ListItemsOptions()
        
        _ = folderApi?.listItems(withAccountId: accountID, folderId: self.folderID, options: options, completionHandler: { (items, _) in
            if let folderItems = items?.folderItems as? [DSFolderItem]{
                let filterItems = folderItems.filter{ $0.status != "completed" && $0.status != "voided" &&  $0.status != "declined" }
                self.indoxCount = filterItems.count
                completionHandler(filterItems)
            } else {
                completionHandler([])
            }
        })
    }
    
    typealias displayCompletionHandler = (String?, Error?) -> Void
    func displayTemplateForSignature(_ templateId: String, controller: UIViewController, url: URL?, completion: @escaping displayCompletionHandler) {

        let jsonString = getHeaderJsonString()
        let apiClient = DSApiClient(baseURL: baseUrl)
        let envelopApi = DSEnvelopesApi(apiClient: apiClient)
        envelopApi?.setDefaultHeaderValue(jsonString, forKey: headerName)
        let options = DSViewLinkRequest()
        options.email = "jonathanclayton1@gmail.com"
        options.returnUrl = AppConstant.docusignHost?.absoluteString

        let request = DSRecipientViewRequest()
        request.authenticationMethod = "email"
        request.email = email
        request.userName = userName
        request.returnUrl = AppConstant.docusignHost?.absoluteString
        _ = envelopApi?.createRecipientView(withAccountId: self.accountID, envelopeId: templateId, recipientViewRequest: request, completionHandler: { (url, error) in
            completion(url?.url, error)
        })
        
    }
}
